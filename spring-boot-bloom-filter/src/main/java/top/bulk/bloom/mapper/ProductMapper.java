package top.bulk.bloom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.bulk.bloom.entity.Product;

/**
 * 产品demo(Product)表数据库访问层
 *
 * @author 散装java
 * @since 2022-12-08 18:00:10
 */
public interface ProductMapper extends BaseMapper<Product> {

}

